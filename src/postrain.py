import sys
from collections import Counter
from collections import defaultdict
import pickle
import codecs


sys.path.append('../')
import perceplearn


def postrain(paths):
	fin = codecs.open(paths[1],'r', 'latin_1')
	fdummy = 'p:none '
	ldummy = ' n:none'

	sin = str()
	auxin = str()  # small buffer to hold the words following the class
	curtag = str()
	pretag = str()
	nextag = str()
	prewrd = str()
	preprewrd = str()

	#it's vs its
	#you're vs your
	#they're vs their
	#loose vs lose
	#to vs too	

	wtask = ['to','too','it\'s','its','your','you\'re','their','they\'re','loose','lose']
	Wtask = ['To','Too','It\'s','Its','Your','You\'re','Their','They\'re','Loose','Lose']

	wtask += Wtask

	# p - previous
	# c - current
	# n - next
	for line in fin:
		try:
			ltmp = line.split()

			fcur = ltmp[0].split('/')[0]
			if fcur in wtask:

				curtag = ltmp[0].split('/')[1]
				pretag = 'PTAG'
				nextag = 'NTAG'
				prepretag = 'PPTAG'
				preprewrd = 'PPWRD'


				if (len(ltmp) > 1):
					nextag = ltmp[1].split('/')[1]
					auxin = fdummy + ' n:' + ltmp[1].split('/')[0] + ' pt:' + pretag + ' nt:' + nextag + ' ppt:' + prepretag + ' pp:' + preprewrd +'\n'
					sin += fcur + ' ' + auxin
				else:
					auxin = fdummy + ldummy + ' pt:' + pretag +  ' nt:' + nextag + ' ppt:' + prepretag + ' pp:' + preprewrd + '\n'
					sin += fcur + ' ' + auxin
					continue

			wclass = str() # the class

			for pwrd, cwrd, nwrd in zip(ltmp, ltmp[1:], ltmp[2:]):

				fcur = cwrd.split('/')[0]

				curtag = cwrd.split('/')[1]
				pretag = pwrd.split('/')[1]
				nextag = nwrd.split('/')[1]				

				if fcur in wtask:

					auxin = 'p:' + pwrd.split('/')[0]
					auxin += ' n:' + nwrd.split('/')[0] 
					auxin += ' pt:' + pretag +  ' nt:' + nextag + ' ppt:' + prepretag + ' pp:' + preprewrd + '\n'


					sin += fcur + ' ' + auxin

				prepretag = pretag
				preprewrd = pwrd.split('/')[0]


			fcur = ltmp[len(ltmp)-1].split('/')[0]

			if fcur in wtask:

				curtag = ltmp[len(ltmp)-1].split('/')[1]
				pretag = ltmp[len(ltmp)-2].split('/')[1]
				nextag = 'NTAG'
				fpre = ltmp[len(ltmp)-2].split('/')[0]

				auxin =  ' p:' + fcur + ldummy + ' pt:' + pretag +  ' nt:' + nextag + ' ppt:' + prepretag + ' pp:' + preprewrd + '\n'
				sin += fcur + auxin
		except:
			print (line)

	#print (sin)
	finterw = codecs.open('pos_inter','w','latin_1')
	finterw.write(sin)
	#perceplearn.perceplearnfn('pos_inter',paths[2],'pos.dev')
	perceplearn.perceplearnfn('pos_inter',paths[2])

	fin.close()
	finterw.close()


paths = list()
for arg in sys.argv:
	paths.append(arg)

postrain(paths)