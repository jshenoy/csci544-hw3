import sys
from collections import Counter
from collections import defaultdict
import pickle
import codecs
import argparse

correct = codecs.open('hw3.dev.txt','r','latin_1')
error = codecs.open('hw3.dev.err.txt','r','latin_1')
mine = codecs.open('hw3.out.txt','r','latin_1')

totalerr = 0
points  = 0
messed = 0

for lc, le, lm in zip(correct,error,mine):

	cline = lc.split()
	eline = le.split()
	mline = lm.split()


	for cword, eword, mword in zip(cline, eline, mline):

		if cword != eword:
			totalerr += 1

			if cword == mword:
				points += 1
		if cword == eword:
			if mword !=cword:
				messed += 1



print ('Total: ' + str(totalerr) )
print ('Points: ' + str(points) )
print ('Rate: ' + str(points/totalerr) )
print ('Messed: ' + str(messed) )
