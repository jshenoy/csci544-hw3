import sys
from collections import Counter
from collections import defaultdict
import pickle
import codecs

def get_argmax_tag(perceptron, words, fcur, fcurrection):
	vcur = 0
	vcurrection = 0
	for w in words:
		vcur += perceptron[fcur][w]

	for w in words:
		vcurrection += perceptron[fcurrection][w]

	if vcurrection >= vcur:
		return fcurrection
	else:
		return fcur



def get_key(_key, _map):

	if _key in _map:
		return _key
	else:
		return ''
	

debug = 0



def postagfn2(path1):
	wavg = pickle.load(open(path1,"rb"))

	fdummy = 'p:none '
	ldummy = ' n:none'

	auxin = str()  # small buffer to hold the words following the class
	curtag = str()
	pretag = str()
	nextag = str()
	preprewrd = str()

	addspace = False

	# p - previous
	# c - current
	# n - next
	right = 0
	count = 0
	for line in sys.stdin.readlines():

		if not line.strip():
			print (line, end ='')
			continue

		if line.endswith(' ') or line.endswith(' \n'):
			addspace = True
		ltmp = line.split()

		#it's vs its
		#you're vs your
		#they're vs their
		#loose vs lose
		#to vs too

		map_txts = dict()
		map_txts_rev = dict()
		match =str()
		fcurrection = str()
		taggedline = str()

		map_txts['its'] = 'it\'s'
		map_txts['your'] = 'you\'re'
		map_txts['lose'] = 'loose'
		map_txts['too'] = 'to'

		map_txts['Its'] = 'It\'s'
		map_txts['Your'] = 'You\'re'
		map_txts['Lose'] = 'Loose'
		map_txts['Too'] = 'To'

		map_txts_rev['it\'s'] = 'its'
		map_txts_rev['you\'re'] = 'your'
		map_txts_rev['loose'] = 'lose'
		map_txts_rev['to'] = 'too'

		map_txts_rev['It\'s'] = 'Its'
		map_txts_rev['You\'re'] = 'Your'
		map_txts_rev['Loose'] = 'Lose'
		map_txts_rev['To'] = 'Too'

		fpre = fdummy
		fcur = ltmp[0].split('_')[0]

		curtag = ltmp[0].split('_')[1]
		pretag = 'PTAG'
		nextag = 'NTAG'
		prepretag = 'PPTAG'
		preprewrd = 'PPWRD'


		# todo: I need to preserve format of the correction
		if get_key(fcur,map_txts):
			fcurrection = map_txts[fcur]
		if get_key(fcur,map_txts_rev):
			fcurrection = map_txts_rev[fcur]


		if fcurrection:
			if (len(ltmp) > 1):
				nextag = ltmp[1].split('_')[1]
				auxin = fdummy + ' n:' + ltmp[1].split('_')[0] + ' pt:' + pretag +  ' nt:' + nextag + ' ppt:' + prepretag + ' pp:' + preprewrd + '\n'
			else:
				auxin = fdummy + ldummy + ' pt:' + pretag +  ' nt:' + nextag + ' ppt:' + prepretag + ' pp:' + preprewrd + '\n'
				answer = get_argmax_tag(wavg,auxin.split(),fcur, fcurrection)

				if answer == fcurrection: # possible bug
					answer = fcurrection
				else:
					answer = fcur

				taggedline = answer

				if addspace:
					addspace = False
					taggedline += ' '

				print (taggedline)
				continue

			answer = get_argmax_tag(wavg,auxin.split(),fcur, fcurrection)

			if answer == fcurrection: # possible bug
				answer = fcurrection
			else:
				answer = fcur

		else:
			answer = fcur
			# Otherwise last letter repeated twice
			if (len(ltmp) <= 1):
				taggedline = answer

				if addspace:
					addspace = False
					taggedline += ' '
					
				print (taggedline)
				continue

		taggedline = answer

		for pwrd, cwrd, nwrd in zip(ltmp, ltmp[1:], ltmp[2:]):

			fcur = cwrd.split('_')[0]
			fcurrection = ""

			if get_key(fcur,map_txts):
				fcurrection = map_txts[fcur]
			if get_key(fcur,map_txts_rev):
				fcurrection = map_txts_rev[fcur]

			curtag = cwrd.split('_')[1]
			pretag = pwrd.split('_')[1]
			nextag = nwrd.split('_')[1]

			if fcurrection:
				auxin = 'p:' + pwrd.split('_')[0]
				auxin += ' n:' + nwrd.split('_')[0]
				auxin += ' pt:' + pretag +  ' nt:' + nextag + ' ppt:' + prepretag + ' pp:' + preprewrd + '\n'


				answer = get_argmax_tag(wavg,auxin.split(),fcur,fcurrection)

				if answer == fcurrection: # possible bug
					answer = fcurrection
				else:
					answer = fcur

			else:
				answer = fcur

			prepretag = pretag
			preprewrd = pwrd.split('_')[0]


			taggedline += ' ' + answer


		fcurrection = ''
		fcur = ltmp[len(ltmp)-1].split('_')[0]

		if get_key(fcur,map_txts):
			fcurrection = map_txts[fcur]
		if get_key(fcur,map_txts_rev):
			fcurrection = map_txts_rev[fcur]

		answer = ''

		if fcurrection:
			fpre = ltmp[len(ltmp)-2].split('_')[0]
			fnex = ldummy

			curtag = ltmp[len(ltmp)-1].split('_')[1]
			pretag = ltmp[len(ltmp)-2].split('_')[1]
			nextag = 'NTAG'

			auxin =  ' p:' + fpre + fnex + ' pt:' + pretag +  ' nt:' + nextag + ' ppt:' + prepretag + ' pp:' + preprewrd + '\n'
			answer = get_argmax_tag(wavg,auxin.split(),fcur,fcurrection)

			if answer == fcurrection: # possible bug
				answer = fcurrection
			else:
				answer = fcur
		else:
			answer = fcur


		taggedline += ' ' + answer

		if addspace:
			addspace = False
			taggedline += ' '

		print (taggedline)
		sys.stdout.flush()


if __name__=='__main__':
	sys.exit( postagfn2(sys.argv[1]) )
#paths = list()
#for arg in sys.argv:
#	paths.append(arg)

#postag(paths)