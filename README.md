# Error correction using POS tagger #

I used POS taggers to achieve this error correcting code. I used a combination of my modified POS tagger from previous assignment -- which is used to train the POS tagger (postrain.py) and correct the errors (postag.py) and Stanford Core NLP -- which is used to tag both the training data and the error file.

Approach used in the code: I noticed that the tag of a word is determined by what words are around it. So, considering this, if a perceptron is trained such that it can predict the tag of those 10 words (Not considering cases for now. The code handles it.) based on what is around it, then the correct word can be predicted. This approach gives up to 90% accuracy on the dev set. Here the classes are these 10 words.

Another approach: Compare which of the words e.g. to and too have the highest probability during classification. The central point to this idea is that when a text is POS tagged, the tags of each of the 10 words (homonyms) are different e.g. to is tagged TO and too is tagged RB. So the probability of  'to' being classified as TO is more than it being classified as RB. First, get probability of the current word's class ( to's class being TO), then get the probability of the current word's replacement ( too). The word with the class having highest probability wins. This approach gives up to 60% accuracy on the dev set. Here the classes are POS tags. Test file need not be tagged in this approach.

## Features used ##
<previous_to_previous_tag> <previous_tag> <previous_word> <next_word> <next_tag><previous_to_previous_word>



## Training##
1. I used the wikipedia corpus for the training file Download wikipedia-english xml file and convert it to text.
2. Clean it by removing special characters and html tags by using sed/awk or text editor. I used regex of text editor to achieve this.
3. Tag it using Stanford POS tagger. More details of tagging in the next section
4. Replace all "_" to "/"
5. Use the tagged data to generate model using postrain.py: python3 postrain.py hw3.training.txt pos.m


## Error Correction ##
1. Download Stanford Core NLP (Full) and Stanford POS tagger.
2. Copy stanford-corenlp-3.5.1.jar from Stanford Core NLP to Stanford POS tagger.
3. Modify stanford-postagger.sh -- Add option -tokenize false
4. Even after supplying this option, the POS tagger doesn't behave the way we want it to behave. So, I used regex of SublimeText to replace:
    1. Lines with no content in them by a unique dummy word: e.g. ABCDEFGHIJ
    2. Lines with space at the end of it with another dummy word: e.g. ENDSPACE
5. Run: sh stanford-postagger.sh models/english-left3words-distsim.tagger hw3.test.err.txt > hw3_testagged.txt
6. Now use regex to change back ENDSPACE to a space, and ABCDEFGHIJ to newline
7. Use the tagged file hw3_testagged.txt to tag the output by running postag.py: python3 postag.py pos.m < hw3_testagged.txt > hw3.output.txt


## Calculating error rate ##
* Method 1: Count number of too,to, .. etc. in the correct file and find how many did we get right in the output file. Error_rate = output_error/total_count
* Method 2: Count number of errors in the error file, find out how many you fixed and find out how many you messed up.